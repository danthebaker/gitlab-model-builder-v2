#!/usr/bin/env bash
set -e
echo -n $REGISTRY_PASSWORD | docker login --username $REGISTRY_USER --password-stdin registry.gitlab.com
echo $DOTSCIENCE_MODEL_IMAGE_NAME
docker build --build-arg model_name=model -t $DOTSCIENCE_MODEL_IMAGE_NAME .
docker push $DOTSCIENCE_MODEL_IMAGE_NAME

echo
echo "-------------------------------------------------------------"
echo
echo "Your model has been built - you can see the dotscience build"
echo "status in the https://cloud.dotscience.com/models/builds page"
echo
echo "-------------------------------------------------------------"
echo